# [practice.using.technology/card](http://practice.using.technology/card)

This repository contains all sources
- Hardware [design data](Design%20data/Hardware%20design) and [documentation](Documentation/Hardware%20design/Hardware_design.md) (work in progress)
- Client side software source code (TODO) and design documentation (TODO)
- Server side software source code (TODO) and design documentation (TODO)

of [practice.using.technology/card](http://practice.using.technology/card).
Feel free to open an [issue](../../../issues) or, even better, implement a proposed solution by creating a [pull request](../../../pulls).