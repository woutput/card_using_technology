__Hardware design document (work in progress)__

# Design decisions
These design decisions follow the [mission, vision and goals](http://card.using.technology/en/mission/).
## Level 1
- DD1.1. Be able to ship the practice.using.technology/card inside an envelope just like a traditional greeting card (to meet vision "easily available", mission "share the fun" and goal "Provide practice.using.technology/card")
- DD1.2. Minimize cost (to meet vision "easily available")
- DD1.3. Have sound and light effects (to meet mission "the fun")
- DD1.4. Have sufficient runtime using a standard battery (to meet vision "easily available")
- DD1.5. Maximize options for makers to modify and extend the practice.using.technology/card (to meet mission "the fun" and vision "to learn from")
## Level 2
- DD2.1. Minimize weight, preferably less than 50 grams, even better would be less than 20 grams (to meet DD1.1)
- DD2.2. Minimize thickness, preferably less than 5 mm (to meet DD1.1)
- DD2.3. Adhere to height and width requirements: 140 x 90 <= H x W <= 297 x 210 mm (**TODO**: add references to rules) (to meet DD1.1)
- DD2.4. Use low cost, low-power microcontroller, LEDs and piezo buzzer (to meet DD1.2, DD1.3, DD1.4 and DD1.5)
- DD2.5. Create holes in the PCB to be compatible with [LEGO](https://www.lego.com)® bricks[^1] (to meet RQ5)
## Level 3
- DD3.1. Use PCB-only solution instead of component where possible (to meet DD1.2, DD2.1, DD2.2)
- DD3.2. Use small SMD components on one side: top (to meet DD2.1, DD2.2)
- DD3.3. Use a PCB size of 140 x 90 mm (to meet DD1.2, DD2.1 and DD2.3)
- DD3.5. Use zero-Ohm resistors that can be removed easily to give the microcontroller pins another purpose (to meet DD1.5)
- DD3.6. Allow programming of the microcontroller (to meet DD1.5)
- DD3.7. Allow easy access to all pins of the microcontroller (to meet DD1.5)
# Component selection
## Microcontroller
- A microcontroller in the [Microchip (formerly Atmel) AVR series](https://www.microchip.com/en-us/products/microcontrollers-and-microprocessors/8-bit-mcus/avr-mcus) was chosen because of DD4, the available documentation and open source libraries and tools.
- To support USB on such a microcontroller, [V-USB](https://www.obdev.at/products/vusb/index.html) is chosen. V-USB *"Runs on any AVR microcontroller with at least 2 kB of Flash memory, 128 bytes RAM and a clock rate of at least 12 MHz."* [This parametric search](https://www.microchip.com/en-us/parametric-search/chartno_716) filtered on "8-bit AVR" and [this table](https://en.wikipedia.org/wiki/ATtiny_microcontroller_comparison_chart) led to the choice for the [ATtiny828](https://www.microchip.com/en-us/product/ATtiny828)
- One important requirement is the minimum frequency of 12 MHz which is required at the supplied 3 V. This information can only be found in the datasheets.
- To control many LEDs, a scheme such as [(traditional) multiplexing](https://en.wikipedia.org/wiki/Multiplexed_display) or [Charlieplexing](https://en.wikipedia.org/wiki/Charlieplexing) can be used. Although this reduces the required number of pins to control the LEDs, it also reduces the average brightness. To compensate for the lower average brightness, higher brightness during the on-time is required. For low current/brightness the relation between current and brightness is linear, but for higher current/brightness the efficiency is lower. So the power consumption would increase. Therefore, a microcontroller with many pins is chosen such that each LED can be controlled individually.
## Power supply decoupling
Typical [ceramic capacitors](https://en.wikipedia.org/wiki/Ceramic_capacitor) are chosen because these are small, cheap, can handle high frequencies and are available at a high capacitance. The [chosen devices](Datasheets/Samsung-Electro-Mechanics-CL05A106MQ5NUNC.pdf) have a high capacitance of 10 &#181;F in a small 0402 package and handle a maximum voltage of 6.3 V which is well over the required 3.0 V (microcontroller) and 5.25 V (USB).
## USB
[USB 2.0 specification](Datasheets/usb_20.pdf) p179 states that VOH shall be 2.8...3.6 V. The microcontroller runs at 3.0 V so both incoming and outgoing voltages are within specification.
### Connector
#### Option A: SMD connector
![USB SMD connector](USB%20connector%20-%20Option%20A.png)
#### Option B: PCB-only
![USB PCB-only](USB%20connector%20-%20Option%20B.png)
#### Selection
The PCB-only "connector" is chosen to minimize weight, total thickness and cost. The [USB specification](Datasheets/CabConn20.pdf) specifies the dimensions of the micro-USB B-plug and receptacle on pages 41...44. This requires the PCB to have a thickness of 0.6 mm which also reduces the weight of the PCB compared to the default thickness of 1.6 mm. This is inspired by [this repository](https://github.com/Pinuct/Eagle_PCB_USB_connectors) which is used in [this design](https://github.com/Pinuct/Business-Card-Gamepad) to create [this business card gamepad](https://hackaday.io/project/28516-business-card-gamepad)
**Risk: The PCB-only USB connector may break easily or may have a low amount of mating cycles. This should be tested.**
### Pull-up dataline
*"Low-speed devices are terminated ... with the pull-up resistor on the D- line."* *"... a 1.5 kΩ ±5% resistor tied to a voltage source between 3.0 V and 3.6 V ..."* according to [USB 2.0 specification](Datasheets/usb_20.pdf) paragraph 7.1.5.1, page 141. The D- line is pulled-up to 3.0 V by using a voltage devider on the USB-supplied 5.0 V line. This allows the microcontroller to measure whether a USB host device is connected or not by looking at D-.
### Series resistors
Existing implementations ([Digispark schematic 1](Other%20projects/DigisparkSchematic.pdf), [Digispark schematic 2](Other%20projects/DigisparkSchematicFinal.pdf) and [Arduino uno](Other%20projects/arduino-uno-schematic.pdf)) use 22...68 Ohms. The [V-USB](https://www.obdev.at/products/vusb/index.html) [reference design](http://metalab.at/wiki/Metaboard) [uses 68 Ohms](Other%20projects/Metaboard-circuit.gif) and so do all other examples on their website using ATTiny (and ATMega) chips, therefore 68 Ohms is used for the first design.
## Power supply
### USB
Since a USB-connection is available for downloading firmware to the microcontroller, this connection can also be used as power supply input. Note that *"The maximum load (CRPB) that can be placed at the downstream end of a cable is 10 μF in parallel with 44 Ω. The 10 μF capacitance represents any bypass capacitor directly connected across the VBUS lines in the function plus any capacitive effects visible through the regulator in the device. The 44 Ω resistance represents one unit load of current drawn by the device during connect."* according to [USB 2.0 specification](Datasheets/usb_20.pdf) paragraph 7.2.4.1, page 177.
### Battery
A CR2032 battery is chosen because it is cheap, relatively small, easily available and has a long lifetime. [Here is a datasheet](Datasheets/cr2032.pdf) and [another one](Datasheets/Adafruit_3262.pdf) which contain information that is assumed to be similar for other brands.
#### Measurement 
**TODO**:Add many measurement results
#### Holder
#### Option A: SMD battery holder
![SMD battery holder](Battery%20holder%20-%20Option%20A.png)
#### Option B: PCB-only
![PCB-only battery holder](Battery%20holder%20-%20Option%20B.png)
#### Selection
A PCB-only battery holder is used to minimize weight, thickness and cost.
### Power supply selection
Since there are two power supply options (USB and battery), the circuit must either be able to handle the connection of both power supplies at the same time or allow the selection of one or the other.
#### Automatic power supply selection (OR-ing power supplies)
Using two diodes to combine the power supplies is possible, but creates a significant (especially on the 3V battery), and load current dependent, voltage drop. Using a MOSFET as ideal diode is only possible for reverse voltage protection, but not for OR-ing multiple power supplies. Dedicated ICs for OR-ing power supplies are available but add a significant cost and idle power consumption. Therefore manual power selection is chosen.
#### Manual power selection
To allow the selection of a power supply, a simple PCB-only solder jumper is used. This option is very cheap and very energy efficient. A small disadvantage is that the user has to modify the circuit to select USB as the power supply. Another disadvantage is the risk that the user accidentally connects both power supplies. In that case, the 3V output of the buck converter may create a current into the CR2023 battery or the other way around: the battery may create a current going into the buck converter. [This datasheet](Datasheets/Adafruit_3262.pdf) states that a maximum current of 10 mA is tolerable. Since the output voltage of the DC-DC converter will be close to that of the battery, only a resistor with a low value would be needed. This resistor is omitted since a CR2032 already has a high internal resistance. In addition, this would only be needed in case of two user errors (connect both power sources and also select both power sources).
## Connection to all signals
Most signals of the design, including all signals from the microcontroller (except for the USB signals) are easily accessible to allow extensions and modifications.
### Extension connector
A single row, 32 pins, 2.54 mm header is used since it is commonly used and cheap. It is placed in a zigzag pattern (inspired by [this repository](https://github.com/avandalen/Connectorless-zigzag-1x6-mini-ISP-connector)) so that a connection can be made by only pushing a (small) header into the holes without soldering. Small pressure is applied to the pins thanks to the zigzag pattern. This makes sure that the resistance between the pins and the holes is low and the connection is reliable. This connection is also used to program the bootloader onto the microcontroller for the first time during production using in-circuit serial programming (ICSP). From then onwards, the USB connector can be used to download new firmware using the bootloader.
### 0 Ohm resistors
0 Ohm resistors are connected between the microcontroller on one side and the devices (LEDs, buzzer and reset button) on the other side. If a 0 Ohm resistor is removed using a soldering iron or simply by using cutting pliers, the connection is broken. This may be needed when the pin is used for another function through the extension connector.
0805 package should be workable for most people when soldering by hand.
## Buzzer
To maximize the sound level from the buzzer, both of its pins are connected to the microcontroller. The pins are connected in the same bank so that both pins can be changed with one (assembly) command. This configuration makes it possible to have one pin at 3V while the other pin as at 0V in one halve of the cycle, while the first pin gets 0V and the other pin gets 3V in the other halve of the cycle. The effect of this is a 6 Vpp square wave. This is a larger voltage swing, so louder noise, than the configuration where only one pin of the buzzer is connected to the microcontroller (which can generate a 3 Vpp square wave with a 1.5 V DC offset which reduces the lifetime of the piezo element inside the buzzer).
## LEDs
### LEDs themselves
**TODO**
- VFtypical 2.8 V @ IF 5 mA: Warm white*, White, Pink*, Violet, Blue*, Ice blue*, True green
- VFtypical 1.8 V @ IF 5 mA, so 200 Ohm resistors (0402 package): Yellow green, Yellow*, Orange*, Red*
### LED resistors
**TODO**: Calculate LED resistors: resistance and power loss (so minimum package)
## Reset
AN2519 p8: Series resistor for button on reset to avoid spike on reset pin
ATtiny828 full datasheet p248: internal reset pull-up resistor is 30...60 kOhm
### Reset button
#### Option A: PCB-only touch sensor using uC
**TODO**

#### Option B: PCB-only touch sensor using external IC
**TODO**

#### Option C: PCB-only reset button using PCB traces and transistor
**TODO**

#### Option D: SMD button
**TODO**

# PCB
**TODO**: holes, rounded corners, etc.
**TODO**: Consider adding holes for (shoe) charms, see [here](https://www.thingiverse.com/thing:9492), [here](https://www.crocs.com/c/jibbitz) and [here](https://www.aliexpress.com/wholesale?SearchText=shoes+charms)

# TODO
**TODO** Run KiCAD DRCs
**TODO** View Gerber files in multiple different viewers and check for errors
**TODO** Look for silkscreen that overlaps with other silkscreen or other layers
**TODO**: Calculate/simulate voltages, currents and power usage of all components. For example, check if resistor package is large enough:
- 01005: 31 mW
- 0201: 50 mW
- 0402: 63 mW
- 0603: 100 mW
- 0805: 125 mW
- 1206: 250 mW

# Risks
* The PCB-only USB connector may break easily or may have a low amount of mating cycles. This should be tested.


[^1]: LEGO® is a trademark of the LEGO Group of companies which does not sponsor, authorize or endorse this site
